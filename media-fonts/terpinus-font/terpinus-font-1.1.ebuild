# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Terminus font set enhanced with Vietnamese glyphs"
HOMEPAGE="http://repo.or.cz/w/terpinus.git"
SRC_URI="http://vinux.sourceforge.net/pclouds/files/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 ~mips ppc ppc64 s390 sh sparc x86 ~x86-fbsd"
IUSE="X"

DEPEND="sys-apps/gawk
		dev-lang/perl
		X? ( || ( x11-apps/bdftopcf virtual/x11 ) )"
RDEPEND=""

src_compile() {
	./configure \
		--prefix=/usr \
		--psfdir=/usr/share/consolefonts \
		--acmdir=/usr/share/consoletrans \
		--unidir=/usr/share/consoletrans \
		--x11dir=/usr/share/fonts/terpinus

	emake psf txt || die

	# If user wants fonts for X11
	if use X; then
		emake pcf || die
	fi
}

src_install() {
	make DESTDIR=${D} install-psf install-acm install-ref || die

	# If user wants fonts for X11
	if use X; then
		make DESTDIR=${D} install-pcf || die
		einfo "Remove ISO8859-1 fonts"
		rm ${D}/usr/share/fonts/terpinus/ter-1*.pcf*
		mkfontdir ${D}/usr/share/fonts/terpinus
	fi

	dodoc README*
}
