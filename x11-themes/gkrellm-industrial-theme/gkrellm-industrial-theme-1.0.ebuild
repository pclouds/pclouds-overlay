# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-themes/gkrellm-themes/gkrellm-themes-0.1.ebuild,v 1.15 2006/01/11 18:31:26 gustavoz Exp $

DESCRIPTION="A pack of ~200 themes for GKrellM"
HOMEPAGE="http://www.muhri.net/gkrellm"
SRC_URI="http://download.freshmeat.net/themes/industrial-gkrellm/industrial-gkrellm-default-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
RESTRICT="nostrip"

DEPEND=""
RDEPEND=">=app-admin/gkrellm-2.1"

S="${WORKDIR}"

src_compile() {
	einfo "Nothing to compile"
}

src_install() {
	dodir /usr/share/gkrellm2/themes/
	keepdir /usr/share/gkrellm2/themes/
	cd ${S}
	ewarn "Please ignore any errors that may appear!"
	chmod -R g-sw+rx *
	chmod -R o-sw+rx *
	chmod -R u-s+rwx *
	cp -pR * ${D}/usr/share/gkrellm2/themes/
}
