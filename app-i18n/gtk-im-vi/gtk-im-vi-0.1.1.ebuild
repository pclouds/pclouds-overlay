# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2 eutils

DESCRIPTION="Simple GTK+ Input Methods module for VNI and TELEX"
HOMEPAGE="http://freshmeat.net/projects/gtk-im-vi"
SRC_URI="http://vinux.sourceforge.net/backup/pclouds/files/${P}.tar.gz"
LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
RDEPEND=">=x11-libs/gtk+-2"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

get_gtk_confdir() {
	if useq amd64 || ( [ "${CONF_LIBDIR}" == "lib32" ] && useq x86 ) ; then
		echo "/etc/gtk-2.0/${CHOST}"
	else
		echo "/etc/gtk-2.0"
	fi
}

pkg_postinst() {
	if [ -x /usr/bin/gtk-query-immodules-2.0 ] ; then
		gtk-query-immodules-2.0 > ${ROOT}/$(get_gtk_confdir)/gtk.immodules
	fi
	gnome2_pkg_postinst
}

pkg_postrm() {
	if [ -x /usr/bin/gtk-query-immodules-2.0 ] ; then
		gtk-query-immodules-2.0 > ${ROOT}/$(get_gtk_confdir)/gtk.immodules
	fi
	gnome2_pkg_postrm
}
