# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake flag-o-matic

DESCRIPTION="A speed and usability focused glTF 2.0 library in C++17"
HOMEPAGE="https://fastgltf.readthedocs.io/latest/"
SRC_URI="https://github.com/spnda/fastgltf/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=">=dev-libs/simdjson-3.9.4"
BDEPEND=">=dev-build/cmake-3.12"

PATCHES=(
	"${FILESDIR}/0001-GNUInstallDirs.patch"
)

IUSE=""
